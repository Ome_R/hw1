#include <iostream>
#include "linkedList.h"

/*
This function adds a value to the top of a linked list.
list - A pointer to pointer to a linked list struct.
*/
void addValue(linkedList** list, unsigned int value) {
	//Creating a new struct that represends the value
	linkedList* newValue = new linkedList;
	linkedList* firstNode = *list;
	//Setting the struct with the correct values
	newValue->value = value;
	newValue->next = (*list);
	//Setting the struct as the header of the list
	*list = newValue;
}

/*
This function deletes a value from the top of a linked list.
list - A pointer to pointer to a linked list struct.
*/
void delValue(linkedList** list) {
	linkedList* firstValue = *list;
	*list = firstValue->next;
	delete(firstValue);
}

/*
This function clears all the linked list from the memory.
*/
void clearList(linkedList** list) {
	linkedList* currentNode = *list, *nextNode = NULL;

	while (currentNode != NULL) {
		nextNode = currentNode->next;
		delete(currentNode);
		currentNode = nextNode;
	}

}

/*
This function creates a new linked list with a value at the start.
value - A value to set the new list with.
return - A pointer to a new linked list.
*/
linkedList* createList(unsigned int value) {
	linkedList* list = new linkedList;
	list->value = value;
	list->next = NULL;
	return list;
}