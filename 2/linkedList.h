#ifndef LINKEDLIST_H
#define LINKEDLIST_H


typedef struct linkedList {
	unsigned int value;
	linkedList* next;
} linkedList;

void addValue(linkedList** list, unsigned int value);
void delValue(linkedList** list);
void clearList(linkedList** list);
linkedList* createList(unsigned int value);

#endif /* LINKEDLIST_H */