#include <iostream>
#include "utils.h"
#include "stack.h"

/*
This function reverses a list of integers
nums - A pointer to an int
size - The size of the list
*/
void reverse(int* nums, unsigned int size) {
	int i = 0, value = 0;
	stack* s = new stack;
	initStack(s);

	//Loading the array into our stack
	for (i = 0; i < size; i++) {
		push(s, nums[i]);
	}

	//Reseting the i value
	i = 0;

	do {
		value = pop(s);
		if (value != -1) {
			nums[i] = value;
			i++;
		}
	} while (value != -1);

	delete(s);
}

int* reverse10() {
	int i = 0;
	int* nums = new int[10];

	for (i = 0; i < 10; i++) {
		std::cin >> nums[i];
	}

	reverse(nums, 10);

	return nums;
}