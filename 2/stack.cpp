#include <iostream>
#include "stack.h"

/*
This function adds an element to a stack.
s - A pointer to a stack
element - An element to add
*/
void push(stack* s, unsigned int element) {
	//Creating a new struct that represends the first value
	stack* secondElement = NULL;

	//
	if (s->value != NULL) {
		secondElement = new stack;
		secondElement->value = s->value;
		secondElement->next = s->next;
	}

	//Updating the header with the wanted element
	//Setting the next node to be the last first one
	s->next = s->value == NULL ? NULL : secondElement;
	s->value = element;
}

/*
This function removes from the stack the last element that was added to it.
s - A pointer to a stack
*/
int pop(stack* s) {
	int returnValue = -1;
	stack* newFirstNode = NULL;

	if (s->value != NULL) {
		returnValue = s->value;
		//The list contains more than 1 value
		if (s->next != NULL) {
			newFirstNode = s->next;
			s->value = newFirstNode->value;
			s->next = newFirstNode->next;
			delete(newFirstNode);
		}
		else {
			//Reseting our stack
			initStack(s);
		}
	}

	return returnValue;
}

/*
This function sets a stack with a default value (resets it).
s - A pointer to a stack
*/
void initStack(stack* s) {
	//We are setting the stack with NULL values
	s->value = NULL;
	s->next = NULL;
}

/*
This function clears a stack from the memory.
s - A pointer to a stack
*/
void cleanStack(stack* s) {
	stack* currentNode = s, *nextNode = NULL;

	while (currentNode != NULL) {
		nextNode = currentNode->next;
		delete(currentNode);
		currentNode = nextNode;
	}

}