#include "queue.h"

/*
This function sets a new size to the queue.
It also resets the maxSize and currentSize values.
q - A pointer to a queue struct.
size - A new size for the queue.
*/
void initQueue(queue* q, unsigned int size) {
	q->queueArray = new unsigned int[size];
	q->maxSize = size;
	q->currentSize = 0;
}

/*
This function clears the entire queue from memory.
q - A pointer to a queue struct.
*/
void cleanQueue(queue* q) {
	delete(q->queueArray);
	delete(q);
}

/*
This function adds a new value to the queue.
q - A pointer to a queue struct.
newValue - A new value to add to the queue.
*/
void enqueue(queue* q, unsigned int newValue) {
	int i = 0;

	if (q->currentSize < q->maxSize) {
		//Setting the new value in the first index
		q->queueArray[q->currentSize] = newValue;
		q->currentSize = q->currentSize + 1;
	}

}

/*
This function removes the first value in the queue.
q - A pointer to a queue struct.
return - The value of the top of the queue (-1 if is empty)
*/
int dequeue(queue* q) {
	int returnValue = -1, i = 0;

	if (q->currentSize > 0) {
		returnValue = q->queueArray[0];

		for (i = 1; i < q->currentSize; i++) {
			q->queueArray[i - 1] = q->queueArray[i];
		}

		q->currentSize = q->currentSize - 1;
	}

	return returnValue;
}
